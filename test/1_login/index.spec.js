import test, { before, after, beforeEach, afterEach } from 'ava'
import Selenium from 'selenium-webdriver'
import Auth from './auth'

beforeEach(t => {
  t.context.driver = new Selenium.Builder().forBrowser('firefox').build()
  t.context.driver.get('https://the-internet.herokuapp.com/login')
  t.context.auth = new Auth(t.context.driver, Selenium.By)
})

afterEach(t => {
  t.context.driver.quit()
})

test('Login - success', async (t) => {
  const credentials = {username: 'tomsmith', password: 'SuperSecretPassword!'}
  const message = await t.context.auth.login(credentials)
  t.true(await message.isLoggedIn())
})

test('Login - incorrect username, correct password', async (t) => {
  const credentials = {username: 'tomsmit', password: 'SuperSecretPassword!'}
  const message = await t.context.auth.login(credentials)
  t.true(await message.isUsernameInvalid())
})

test('Login - correct username, incorrect password', async (t) => {
  const credentials = {username: 'tomsmith', password: 'SuperSecretPassword'}
  const message = await t.context.auth.login(credentials)
  t.true(await message.isPasswordInvalid())
})