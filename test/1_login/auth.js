const Message = require('./message')

class Auth {

  constructor(driver, locator) {
    this.driver = driver
    this.By = locator
    this.elemIDs = {
      username: 'username', 
      password: 'password'
    }
    this.elemClasses = {
      submit: 'radius'
    }
  }

  login({username, password}) {
    this._setValue(this.elemIDs.username, username)
    this._setValue(this.elemIDs.password, password)
    return this.driver.findElement(this.By.className(this.elemClasses.submit)).click()
                      .then(_ => new Message(this.driver, this.By))
  }

  _setValue(elemId, value) {
    this.driver.findElement(this.By.id(elemId)).sendKeys(value)
  }

}

module.exports = Auth