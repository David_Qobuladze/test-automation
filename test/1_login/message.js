class Message {

  constructor(driver, locator) {
    this.driver = driver
    this.By = locator
    this.alertId = 'flash'
    this.success = {
      text: 'You logged into a secure area!',
      state: 'success'
    }
    this.error = {
      username: 'Your username is invalid!',
      password: 'Your password is invalid!',
      state: 'error'
    }
  }


  async isLoggedIn() {
    const text = await this._getAlertText()
    const state = await this._getAlertState()
    return text === this.success.text && state === this.success.state
  }

  async isUsernameInvalid() {
    const text = await this._getAlertText()
    const state = await this._getAlertState()
    return text === this.error.username && state === this.error.state
  }

  async isPasswordInvalid() {
    const text = await this._getAlertText()
    const state = await this._getAlertState()
    return text === this.error.password && state === this.error.state
  }

  _getAlertText() {
    const element = this._getAlertElem()
    return element.getText().then(text => {
      return text.split('\n')[0]
    })
  }

  _getAlertState() {
    const element = this._getAlertElem()
    return element.getAttribute('class').then(classes => classes.split(' ')[1])
  }

  _getAlertElem() {
    return this.driver.findElement(this.By.id(this.alertId))
  }

}

module.exports = Message