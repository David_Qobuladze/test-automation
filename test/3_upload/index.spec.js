import test, { after, beforeEach, afterEach } from 'ava'
import { Builder, By } from 'selenium-webdriver'

beforeEach(t => {
  t.context.driver = new Builder().forBrowser('firefox').build()
  t.context.driver.get('https://the-internet.herokuapp.com/upload')
  t.context.ids = {
    selectorBtn: 'file-upload',
    submitBtn: 'file-submit',
    uploadedPanel: 'uploaded-files',
    dragElem: 'drag-drop-upload'
  }
  t.context.files = {
    file_1: 'file1.txt',
    file_2: 'file2.txt'
  }
  t.context.errorMsg = 'Internal Server Error'
})

afterEach(t => {
  t.context.driver.quit()
})

function getElement(id, driver) {
  return driver.findElement(By.id(id))
}

test('upload from button', async (t) => {
  const ids = t.context.ids
  const driver = t.context.driver
  const files = t.context.files

  await getElement(ids.selectorBtn, driver).sendKeys(`${__dirname}/${files.file_1}`)
  await getElement(ids.submitBtn, driver).click()
  const uploadedFileName = await getElement(ids.uploadedPanel, driver).getText()
  t.is(uploadedFileName, files.file_1)
})

test('upload from drag', async (t) => {
  const ids = t.context.ids
  const driver = t.context.driver
  const files = t.context.files
  const xp = '/html/body/input'
  const errorMsg = 'Internal Server Error'

  await driver.findElement(By.xpath(xp)).sendKeys(`${__dirname}/${files.file_1}`)
  await getElement(ids.submitBtn, driver).click()
  const result = await driver.findElement(By.tagName('h1')).getText()

  t.is(result, errorMsg)
})