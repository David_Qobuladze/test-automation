import test, { beforeEach, afterEach } from 'ava'
import { Builder, By } from 'selenium-webdriver'

beforeEach(t => {
  t.context.driver = new Builder().forBrowser('firefox').build()
  t.context.driver.get('https://the-internet.herokuapp.com/hovers')
})

afterEach(t => {
  t.context.driver.quit()
})

test('hover shows data', async (t) => {
  const elems = await t.context.driver.findElements(By.className('figure'))
  const dataClassName = 'figcaption'
  for (let e of elems) {
    const dataBeforeHover = await isDataDisplayed(e, dataClassName)
    makeHoverOn(e, t.context.driver)
    const dataOnHover = await isDataDisplayed(e, dataClassName)
    const header =  t.context.driver.findElement(By.tagName('h3'))
    makeHoverOn(header, t.context.driver)
    const dataAfterHover = await isDataDisplayed(e, dataClassName)

    t.false(dataBeforeHover)
    t.true(dataOnHover)
    t.false(dataAfterHover)
  }
})

async function makeHoverOn(element, driver) {
  await driver.actions().move({origin: element}).perform()
}

async function isDataDisplayed(parent, className) {
  return await parent.findElement(By.className(className)).isDisplayed()
}