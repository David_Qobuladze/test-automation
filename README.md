
პროექტი დაწერილია javascript-ზე.

გაშვებისთვის აუცილებელია:

**node -- 8.9.4 or later**

**npm -- 6.9.0 or later**

პროექტის ჩამოტვირთვის შემდეგ, working directory-დან გამოსაძახებელია შემდეგი ბრძანება:

```shell 
npm install - ბიბლიოთეკების დაყენება (selenium-webdriver და ava.js)
``` 

ტესტების გაშვებისთვის working directory-დან გამოსაძახებელია შემდეგი ბრძანებები:

```shell 
npm run test-login

npm run test-hover

npm run test-upload
```

```შენიშვნა: კოდი გაშვებისთვის იყენებს firefox driver-ს (geckodriver), რომლის მისამართს ელოდება PATH environment variable-ში```